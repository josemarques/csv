//JOSE_LUIS MARQUES_FERNANDEZ 2015
//CSV PARSER
//OPTIMIZED FOR DEBIAN 8

#ifndef CSV_HPP_INCLUDED
#define CSV_HPP_INCLUDED

/*
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cstring>
#include <sstream>
//#include <stdlib.h>
#include <vector>
#include <fstream>
#include <string>

#include <algorithm>
*/

#include <sstream>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <iomanip>

#include "SUBM/FHL/FHL.hpp"

class CSV{
public:
	int HEADSIZE;
	std::string FILENAME;
	std::string TOKEN;

	std::string STRDATA;
	//std::vector<std::vector<float>> DATA;//Data
	std::vector<std::vector<double>> DATA;//Data
	std::vector<std::vector<std::string>> HEAD;//Head data.

	//CSV();//Constructor sin estado
	CSV(std::string filename="", std::string token=",", int headsize=0);//Constructor con estado
	CSV(const CSV& csv);//Duplicador de clase
	CSV& operator=(const CSV& csv); //Igualador de clase
	~CSV();//Destructor

	void ENCO();//Encode data
	void DECO();//Decode data

	void GFD();//Get file data
	void PFD();//Put file data
	void UFD();//Update file data
};


#endif
