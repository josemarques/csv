#include "../CSV.hpp"

void PRNTVECT(std::vector<std::vector<float>> data){
	for (int i=0; i<data.size(); i++){
		for (int ii=0; ii<data[i].size(); ii++){
			std::cout<<data[i][ii]<<" ";
		}
		std::cout<<std::endl;
	}
}

void PRNTVECT(std::vector<std::vector<std::string>> data){
	for (int i=0; i<data.size(); i++){
		for (int ii=0; ii<data[i].size(); ii++){
			std::cout<<data[i][ii]<<" ";
		}
		std::cout<<std::endl;
	}
}

int main(){
std::cout << "CSV_TST"<<std::endl;

std::cout << "FUNCTION TESTS"<<std::endl;

std::cout << "LOADFILE"<<std::endl;
std::string FILESTRING=LF("TEST.csv");
std::cout<< "FILE CONTENTS::"<< std::endl;
std::cout<<FILESTRING<<std::endl;

std::cout << "SAVEFILE"<<std::endl;
std::string SAVEFUCND="DATA;;SAVE;;C++";
SAVEFUCND=SAVEFUCND+"\n";
SAVEFUCND=SAVEFUCND+"12;252;63.6;2345;aser;;";
SF("TESTS.csv",SAVEFUCND);

std::cout << "APPENDFILE"<<std::endl;
std::string APPENDSTR="\nA;P;E;N;D;E;D";
AF("TESTS.csv",APPENDSTR);

std::cout << "TSSV"<<std::endl;
std::vector<std::vector<std::string>> STRVECT=TSSV(FILESTRING,";");

std::cout << "SVTS"<<std::endl;
std::string STRRECOV=SVTS(STRVECT,";");
std::cout<<STRRECOV<<std::endl;

std::cout << "SVFV"<<std::endl;
std::vector<std::vector<float>> FLTVECT=SVFV(STRVECT);
PRNTVECT(FLTVECT);

std::cout << "FVSV"<<std::endl;
STRVECT=FVSV(FLTVECT);
PRNTVECT(STRVECT);

std::cout << "OBJECT TEST" << std::endl;

std::cout << "OBJECT CREATION" << std::endl;
CSV CSVO ("TESTS.csv",";",0);

std::cout<<("OBJECT_STRDATA")<<std::endl;
std::cout<<(CSVO.STRDATA)<<std::endl;

std::cout<<("OBJECT_DATA")<<std::endl;
PRNTVECT(CSVO.DATA);

std::cout<<("OBJECT_DATA_APPEND")<<std::endl;
CSVO.DATA.push_back({23,54,63.56});
CSVO.ENCO();
std::cout<<(CSVO.STRDATA)<<std::endl;
CSVO.UFD();

std::cout<<("OBJECT_DATA_SAVE")<<std::endl;
CSVO.HEADSIZE=2;
CSVO.HEAD={{"HI","BY","MAY"},{"JI","JA","JO","JUU"}};
CSVO.DATA={{124,63,97654,23},{74,3,678,0},{24,6542,2,}};
CSVO.PFD();
std::cout<<(CSVO.STRDATA)<<std::endl;
/*

print("OBJECT TEST")

	print("OBJECT CREATION")
	CSVO=CSV.CSV("TESTS.csv",";",0)

	print("OBJECT_STRDATA")
	print(CSVO.STRDATA)

	print("OBJECT_DATA")
	print(CSVO.DATA)

	print("OBJECT_DATA_APPEND")
	CSVO.DATA.append([23,54,63.56])
	CSVO.ENCO()
	print(CSVO.STRDATA)
	CSVO.UFD()

	print("OBJECT_DATA_SAVE")
	CSVO.HEADSIZE=2
	CSVO.HEAD=[["HI","BY","MAY"],["JI","JA","JO","JUU"]]
	CSVO.DATA=[[124,63,97654,23],[74,3,678,0],[24,6542,2,]]
	CSVO.PFD()
	print(CSVO.STRDATA)
	*/

}
