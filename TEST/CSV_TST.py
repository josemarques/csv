 # -*- coding: utf-8 -*-
#!/usr/bin/env python
#!/usr/bin/python -u

import sys
sys.path.append('..')
import CSV

def TEST():
	print("CSV_TST")

	print("FUNCTION TESTS")

	print("LOADFILE")
	FILESTRING=CSV.LF("TEST.csv")
	print("FILE CONTENTS::")
	print(FILESTRING)

	print("SAVEFILE")
	SAVEFUCND="DATA;;SAVE;;PYTHON"
	SAVEFUCND=SAVEFUCND+"\n"
	SAVEFUCND=SAVEFUCND+"12;252;63.6;2345;aser;;"
	CSV.SF("TESTS.csv",SAVEFUCND);

	print("APPENDFILE")
	APPENDSTR="\nA;P;E;N;D;E;D"
	CSV.AF("TESTS.csv",APPENDSTR)

	print("TSSV")
	STRVECT=CSV.TSSV(FILESTRING,";")

	print("SVTS")
	STRRECOV=CSV.SVTS(STRVECT,";")
	print(STRRECOV)

	print("SVFV")
	FLTVECT=CSV.SVFV(STRVECT)
	print(FLTVECT)

	print("FVSV")
	STRVECT=CSV.FVSV(FLTVECT)
	print(STRVECT)

	print("OBJECT TEST")

	print("OBJECT CREATION")
	CSVO=CSV.CSV("TESTS.csv",";",0)

	print("OBJECT_STRDATA")
	print(CSVO.STRDATA)

	print("OBJECT_DATA")
	print(CSVO.DATA)

	print("OBJECT_DATA_APPEND")
	CSVO.DATA.append([23,54,63.56])
	CSVO.ENCO()
	print(CSVO.STRDATA)
	CSVO.UFD()

	print("OBJECT_DATA_SAVE")
	CSVO.HEADSIZE=2
	CSVO.HEAD=[["HI","BY","MAY"],["JI","JA","JO","JUU"]]
	CSVO.DATA=[[124,63,97654,23],[74,3,678,0],[24,6542,2,]]
	CSVO.PFD()
	print(CSVO.STRDATA)



if __name__ == '__main__':
	TEST()
