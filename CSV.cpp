#include"CSV.hpp"
//Objective

CSV::CSV(std::string filename, std::string token, int headsize){
	FILENAME=filename;
	TOKEN=token;
	HEADSIZE=headsize;
	GFD();
}

CSV::CSV(const CSV& csv){//Class duplicator
	HEADSIZE=csv.HEADSIZE;
	FILENAME=csv.FILENAME;
	TOKEN=csv.TOKEN;

	STRDATA=csv.STRDATA;//String toketed data
	DATA=csv.DATA;//Data
	HEAD=csv.HEAD;//Head data.
}

CSV& CSV::operator=(const CSV& csv){ //Class equalizer
	HEADSIZE=csv.HEADSIZE;
	FILENAME=csv.FILENAME;
	TOKEN=csv.TOKEN;

	STRDATA=csv.STRDATA;//String toketed data
	DATA=csv.DATA;//Data
	HEAD=csv.HEAD;//Head data.
	return *this;
}

CSV::~CSV(){//Class destructor

}

void CSV::ENCO(){//Encode data
	std::string STHEAD;
	std::string STDATA;
	if(HEADSIZE>0){
			STHEAD=SVTS(HEAD,TOKEN);
			STHEAD=STHEAD+"\n";
	}
	else{
		STHEAD="";
	}

	STDATA=SVTS(FVSV(DATA),TOKEN);
	STRDATA=STHEAD+STDATA;
}

void CSV::DECO(){//Decode data
	HEAD=TSSV(STRDATA,TOKEN);//TSSV all file and store it on HEAD str vector
	DATA=SVFV({HEAD.begin()+HEADSIZE,HEAD.end()});//Numeric data
	if(HEADSIZE>0){
		HEAD={HEAD.begin(),HEAD.begin()+HEADSIZE-1};//Head
	}
	else{
		HEAD.clear();
	}
}

void CSV::GFD(){//Get file data
	STRDATA=LF(FILENAME);//String data
	DECO();
}

void CSV::PFD(){//Put file data
	ENCO();
	SF(FILENAME,STRDATA);
}

void CSV::UFD(){//Update file data
	std::string TOKTSTR="\n"+SVTS(FVSV({DATA[DATA.size()-1]}),TOKEN);
	AF(FILENAME, TOKTSTR);

}
