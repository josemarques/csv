 # -*- coding: utf-8 -*-
#!/usr/bin/env python
#!/usr/bin/python -u

#CSV.py ->CSV data importer and exporter

##Base functions

def LF(filename):#Load file, return file on string format
	FILE= open(filename, "r")
	STRDATA=FILE.read()
	return STRDATA

def SF(filename, strdata):#Save string to file
    FILE = open(filename, "w+")
    FILE.write(strdata)
    FILE.close()

def AF(filename, strdata):#Append string to file
	FILE= open(filename, "a")
	FILE.write(strdata)
	FILE.close()

def TSSV(strdata, token=","):#Toketed string to string vector
	RAW=[]

	RAW=strdata.splitlines( )
	#for line in LINES:
	#	RAW.append(line.rstrip())
	#print(RAW)
	DATA=[]
	for i in range(0,len(RAW)):#Line iterator
		rDATA=RAW[i].split(token)
		DATA.append(rDATA)
	return DATA

def SVTS(data, token=","):#String vector to toketed string
	STRDATA=""
	for i in range(len(data)):

		for ii in range(len(data[i])):

			STRDATA=str(STRDATA)+str(data[i][ii])
			if(ii<len(data[i])-1): STRDATA=str(STRDATA)+str(token)

		STRDATA=str(STRDATA)+"\n"
	return STRDATA

def SVFV(data):#String vector to float vector
	FLDT=[]
	for i in range(len(data)):
		tFLDT=[]
		for ii in range(len(data[i])):
			try:
				FLOATV=float(data[i][ii])
			except:
				#print("An exception occurred")
				#tFLDT.append(0)
				pass
			else:
				tFLDT.append(FLOATV)
		FLDT.append(tFLDT)
	return FLDT


def FVSV(data,sigdigits=6):#Float vector to string vector
	STDT=[]
	for i in range(len(data)):
		tSTDT=[]
		for ii in range(len(data[i])):
			tSTDT.append(str(round(data[i][ii],sigdigits)))
		STDT.append(tSTDT)
	return STDT

##Object implementation

class CSV:
	def __init__(self, filename="", token=",", headsize=0):
		self.HEADSIZE=headsize #Numbers of lines of the header
		self.FILENAME=filename
		self.TOKEN=token

		self.STRDATA=""#CSV data on toketed string format
		self.HEAD=[]#Header of the file, String data
		self.DATA=[]#Numeric data, Float data

		self.GFD()

	def ENCO(self):#Encode HEAD+DATA in to STRDATA
		if(self.HEADSIZE>0):
			STHEAD=SVTS(self.HEAD,self.TOKEN)
			STHEAD=STHEAD+"\n"
		else:
			STHEAD=""

		STDATA=SVTS(FVSV(self.DATA))
		self.STRDATA=STHEAD+STDATA

	def DECO(self):#Decode STRDATA in to HEAD+DATA
		if(self.HEADSIZE>0):
			self.HEAD=TSSV(self.STRDATA,self.TOKEN)[0:self.HEADSIZE]#Header of the file

		self.DATA=SVFV(TSSV(self.STRDATA,self.TOKEN)[self.HEADSIZE:])#Numeric data

	def GFD(self):#Get file data
		self.STRDATA=LF(self.FILENAME)#String data
		self.DECO()

	def PFD(self):#Put file data
		self.ENCO()
		SF(self.FILENAME,self.STRDATA)

	def UFD(self):#Update file data. Adds last data vector entry to file
		TOKTSTR="\n"+SVTS(FVSV([self.DATA[-1]]),self.TOKEN)
		AF(self.FILENAME, TOKTSTR)


